﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvalancheDeadLines : MonoBehaviour
{
    public float delayToDestroy;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject, delayToDestroy);
    }
}
