﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvalancheSpawner :MonoBehaviour {
    public GameObject ballPrefab;
    public Sprite[] sprites;
    public float spawnTime;

    void Start() {
        StartCoroutine(spawnBall());
    }

    private void createBall() {
        GameObject newBall = Instantiate(ballPrefab, transform.position, Quaternion.identity) as GameObject;
        newBall.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
    }

    IEnumerator spawnBall() {
        while (true) {
            createBall();
            yield return new WaitForSeconds(spawnTime);
        }
    }
}
