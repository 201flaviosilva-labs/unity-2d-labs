﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinFlip_FlipCoin : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public Sprite[] sides;
    int currentSide = 0;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        StartCoroutine(Rotate(Random.Range(5, 20)));
    }

    IEnumerator Rotate(int randomRotations)
    {
        float timeTime = 0.0001f;
        int countTimes = 0;

        while (countTimes < randomRotations)
        {
            countTimes++;
            currentSide++;

            float size = 1;
            while (size > 0.1)
            {
                size = size - 0.05f;
                transform.localScale = new Vector3(1, size, 1);
                yield return new WaitForSeconds(timeTime);
            }

            spriteRenderer.sprite = sides[currentSide % 2];

            while (size < 0.99)
            {
                size = size + 0.05f;
                transform.localScale = new Vector3(1, size, size);
                yield return new WaitForSeconds(timeTime);
            }

            yield return new WaitForSeconds(0.00001f);
        }
    }
}
