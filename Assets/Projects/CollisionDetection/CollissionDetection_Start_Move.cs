﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollissionDetection_Start_Move :MonoBehaviour {
    [SerializeField] Rigidbody2D rb2d;
    [SerializeField] bool isHorizontal;
    [SerializeField] float speed;


    private void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Start() {
        if (isHorizontal) {
            rb2d.velocity = new Vector2(1, 0) * speed;
        } else {
            rb2d.velocity = new Vector2(0, 1) * speed;
        }
    }
}
