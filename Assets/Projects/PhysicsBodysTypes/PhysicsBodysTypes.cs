﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsBodysTypes :MonoBehaviour {
    [SerializeField] Rigidbody2D rb2d;
    [SerializeField] float speedX;
    [SerializeField] float speedY;

    private void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Start() {
        rb2d.velocity = new Vector2(speedX, speedY);
    }
}
