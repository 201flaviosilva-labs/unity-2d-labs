﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovementPlayer : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float jumpForce;
    [SerializeField] bool flipX = false; // Sprite looking right

    Rigidbody2D _rigidbody2D;

    // Jump
    private bool isGrounded;
    [SerializeField] Transform groundCheck;
    [SerializeField] float checkRadius;
    [SerializeField] LayerMask whatIsGround;

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // Jump
        if (Input.GetButtonDown("Jump") && isGrounded) _rigidbody2D.velocity = Vector2.up * jumpForce;
    }

    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        float x = Input.GetAxis("Horizontal");
        _rigidbody2D.velocity = new Vector2(x * speed, _rigidbody2D.velocity.y);

        if (flipX == true && x > 0) toggleFlipX();
        else if (flipX == false && x < 0) toggleFlipX();
    }

    void toggleFlipX()
    {
        flipX = !flipX;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }
}
