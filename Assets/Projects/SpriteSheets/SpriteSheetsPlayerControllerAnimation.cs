﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSheetsPlayerControllerAnimation : MonoBehaviour
{
    private Animator animatorController;


    private void Awake()
    {
        animatorController = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown("1")) animatorController.Play("Idle");
        else if (Input.GetKeyDown("2")) animatorController.Play("Run");
        else if (Input.GetKeyDown("3")) animatorController.Play("Attack 1");
        else if (Input.GetKeyDown("4")) animatorController.Play("Attack 2");
        else if (Input.GetKeyDown("5")) animatorController.Play("Attack 3");
        else if (Input.GetKeyDown("6")) animatorController.Play("Crouch");
        else if (Input.GetKeyDown("7")) animatorController.Play("Crouch Attack");
        else if (Input.GetKeyDown("8")) animatorController.Play("Dash");
        else if (Input.GetKeyDown("9")) animatorController.Play("Death");
        else if (Input.GetKeyDown("0")) animatorController.Play("Fast Magic");
    }
}
