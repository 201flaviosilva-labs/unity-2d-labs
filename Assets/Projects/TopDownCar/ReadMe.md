# 2D Top Down Car

## Description

Sistema simples de movimentação de carros top-down 2D simples em Unity

Simple 2D Top Down Car Movement System in Unity

## Links
- [Play](https://201flaviosilva.bitbucket.io/src/Labs/Unity/src/Games/2DSandBox/index.html);
- [Source Code](https://bitbucket.org/201flaviosilva/unity-2d-sandbox/src/master/Assets/Projects/TopDownCar/);

- [Tutorial](https://www.youtube.com/watch?v=DVHcOS1E5OQ);
- [Kenney - Assets](https://kenney.nl/assets/racing-pack);

