﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCarController : MonoBehaviour
{
    [Header("Car Settings")]
    public float accelarationFactor = 5f;
    public float turningFactor = 3.5f;
    public float driftFactor = 0.5f;
    public float maxSpeed = 20f;

    // Local Variables
    float accelarationInput = 0;
    float steeringInput = 0;

    float rotationAngle = 0;

    float velocityVsUp = 0;

    // Components
    Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {

    }
    void Update()
    {

    }

    private void FixedUpdate()
    {
        ApplyEngineForce();
        KillOrthogonalVelocity();
        ApplySteering();
    }

    private void ApplyEngineForce()
    {
        velocityVsUp = Vector2.Dot(transform.up, rb.velocity);

        if (velocityVsUp > maxSpeed && accelarationInput > 0) return;
        if (velocityVsUp < -maxSpeed * 0.5f && accelarationInput < 0) return;
        if (rb.velocity.sqrMagnitude > maxSpeed * maxSpeed && accelarationInput > 0) return;

        if (accelarationInput == 0)
        {
            rb.drag = Mathf.Lerp(rb.drag, 3.0f, Time.fixedDeltaTime * 3);
        }
        else
        {
            rb.drag = 0;
        }

        Vector2 engineForceVector = transform.up * accelarationInput * accelarationFactor;

        rb.AddForce(engineForceVector, ForceMode2D.Force);
    }

    public void KillOrthogonalVelocity()
    {
        Vector2 forwardVelocity = transform.up * Vector2.Dot(rb.velocity, transform.up);
        Vector2 rightVelocity = transform.right * Vector2.Dot(rb.velocity, transform.right);

        rb.velocity = forwardVelocity + rightVelocity * driftFactor;
    }

    private void ApplySteering()
    {
        float minSpeedToTurning = Mathf.Clamp01(rb.velocity.magnitude / 8);

        rotationAngle -= steeringInput * turningFactor * minSpeedToTurning;

        rb.MoveRotation(rotationAngle);
    }

    public void SetInputVector(Vector2 inputVector)
    {
        steeringInput = inputVector.x;
        accelarationInput = inputVector.y;
    }
}
