# Unity Movement (RigidBody vs Translate)

## Description

Um teste de qual poderá ser a melhor movimentação para um jogador

- Metodo 1 - Translate;
- Metodo 2 - RigidBody - AddForce();
- Metodo 3 - RigidBody - Velocity;
- Metodo 4 - RigidBody - MovePosition();

## Links
- [Tutorial](https://www.youtube.com/watch?v=ixM2W2tPn6c);