﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityMovementRigidBodyMovePosition : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rb2d;
    public Vector2 direction;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        direction = new Vector2(h, v);
    }

    private void FixedUpdate()
    {
        movePlayer();
    }

    void movePlayer()
    {
        rb2d.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
    }
}
