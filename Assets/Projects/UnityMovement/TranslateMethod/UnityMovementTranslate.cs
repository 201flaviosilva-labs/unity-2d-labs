﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityMovementTranslate : MonoBehaviour
{
    public float speed = 5f;

    void Start()
    {

    }

    void Update()
    {
        movePlayer();
    }

    void movePlayer()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector2 direction = new Vector2(h, v);
        transform.Translate(direction * speed * Time.deltaTime);
    }
}
